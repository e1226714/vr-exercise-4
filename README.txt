building instructions:

server:
disable both PlayerControllers and activate ServerCamera
at boxes disable leapGrap and ViveGrap

leap client:
disable PlayerController for vive and the serverCamera, 
activate leap PlayerController
at boxes activate LeapGrap but keep ViveGrap deactivated

vive client:
disable PlayerController for leap and the serverCamera, 
activate vive PlayerController
at boxes activate ViveGrap but keep LeapGrap deactivated


BoxSpawner is not use and has to be deactivated for all builds