﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class boxSpawner : NetworkBehaviour
{

    // will not be used - just for testing
    [SerializeField]
    private LocalPlayerController localPlayerController;

    public GameObject boxPrefab;

    public int number;

    public override void OnStartServer()
    {
        base.OnStartServer();
        for (int i = 0; i < number; i++)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-0.5f, 0.5f), 1.0f, Random.Range(-.5f, 0.5f));
            Quaternion spawnRotation = Quaternion.Euler(0.0f, Random.Range(0.0f, 180.0f), 0);
            GameObject box = (GameObject)Instantiate(boxPrefab, spawnPosition, spawnRotation);
            ViveGrab vg = box.AddComponent<ViveGrab>() as ViveGrab;
            vg.localPlayerController = this.localPlayerController;
            NetworkServer.Spawn(box);
        }
    }
}
